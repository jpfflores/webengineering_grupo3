'use strict'

// todo: this naive implementation uses mostly singletons and does eager object creation
// consider replacing this with a full-blown IoC library
const config = require('./config');
const pck = require('../package');
const logger = require('./logger').createLogger(config, pck);
const createAdapter = (name) => require(`./adapters/${name}`).createAdapter(config[name]);
const adapters = {
    github: createAdapter('github'),
    bitbucket: createAdapter('bitbucket'),
    stackoverflow: createAdapter('stackoverflow'),
};
const resources = [
    require('./resources/healthcheck'),
];

const schema = require('./graphql').createSchema(config, adapters);

const server = require('./server').createServer(config, logger, schema);

module.exports = {
    resolveConfig: () => config,
    resolveLogger: () => logger,
    resolveAdapters: () => adapters,
    resolveServer: () => server,
}