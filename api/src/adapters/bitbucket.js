'use strict'

module.exports.createAdapter = (config) => {
    this.systemName = 'bitbucket';
    this.url = config.baseUrl;                

    this.getUserStats = (userName) => {
        return {
            'repo.count': 0,
            'repo.forks': 0,
            'repo.watching': 0
        }
    }
    this.getUserStats = this.getUserStats.bind(this);

    return this;
}
