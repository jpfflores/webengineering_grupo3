'use strict'

const got = require('got');

module.exports.createAdapter = (config) => {
    this.systemName = 'github';
    this.url = config.baseUrl;              

    this.getUserStats = (userName) => {
        return {
            'repo.count': 0,
            'repo.stars': 0,
        }
    }
    this.getUserStats = this.getUserStats.bind(this);

    return this;
}
