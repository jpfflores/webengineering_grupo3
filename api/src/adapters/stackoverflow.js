'use strict'

module.exports.createAdapter = (config) => {
    this.systemName = 'stackoverflow';
    this.url = config.baseUrl;                

    this.getUserStats = (userName) => {
        return {
            'questions.answered': 0,
        }
    }
    this.getUserStats = this.getUserStats.bind(this);

    return this;
}
