Feature: User mgmt API

   As an API consumer
   I want to be able to manage user data

   Scenario: Add user
   Given a valid user payload
   When I perform an addUser mutation
   Then I receive a 200 status code
   And the payload for the new resource   

   Scenario: Delete user
   Given an existing user
   When I perform a DeleteUser mutation
   Then I receive a 200 status code

   Scenario: Edit user
   Given an existing user
   And I want to update his/her name
   When I perform an EditUser mutation
   Then I receive a 200 status code
   And the user has the new name
   
   Scenario Outline: User listing
   Given a set of existing users
   And I want to filter them by <filter>
   When I perform query against the users collection
   Then I receive a 200 status code
   And a list of users that match the same <filter>

   Examples:
   | filter      |
   | city        |
   | subdivision |
   | country     |
