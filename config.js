'use strict'

require('dotenv').config();
const config = { }
const e = process.env;
config.mongodbUrl = e.MONGODB_URL || 'localhost:27017/geekrep';
config.oplogConnectionString = e.MONGODB_OPLOG_URL || 'mongodb://127.0.0.1:27017/local';
config.hostname = e.HOST || 'localhost';
config.port = e.PORT || 8080;

config.auth0Domain = e.AUTH0_DOMAIN;
//fixme: turn this to a dev audience
config.auth0audience = e.AUTH0_AUDIENCE || 'https://geekrep.com';
config.logLevel = e.LOG_LEVEL || 'info';
if ((e.NODE_ENV === 'development') && (e.DISABLE_AUTH === 'true')) {
    config.disableAuth = true;
}

module.exports = config;