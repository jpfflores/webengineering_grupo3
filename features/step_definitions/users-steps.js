'use strict'

var { defineSupportCode } = require('cucumber');

defineSupportCode(function ({ Given, Then, When }) {

    Given(/^a ([\w|\s]+) payload$/, function(fixture){
    this.payload = { body : JSON.stringify(require('../fixtures/valid-user'))};
    });
    
    When(/^I perform a (PUT|POST) against the (.*) endpoint$/, function(verb,endpoint){
    	const that = this;
    	return this.doOp(verb,endpoint,this.payload)
    	.then(
    		res => that.response = R.assoc("body",JSON.parse(res.body),res)
    	);
    });
    
    When(/^I perform a (GET|DELETE|PATCH) against the (.*) endpoint$/, function(verb,endpoint){
    	const that = this;
    	return this.doOpWp(verb,endpoint)
    	.then(
    		res => that.response = R.assoc("body", res.body ? JSON.parse(res.body) : {} )
    	);
    });
    
});

