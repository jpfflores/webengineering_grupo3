const {defineSupportCode} = require('cucumber');
const got = require('got');
const R = require('ramda');
const factory = require('../../src/factory');

const cfg = factory.resolveConfig();
const logger = factory.resolveLogger();
const server = factory
    .resolveServer()
    .then((s) => s.start());

function CustomWorld() {
    this.server = server;
    
    function addHeader(header, value, optionsObj) {
        return R.assocPath(["headers", header], value, optionsObj || { });
    }

    function getAuthToken() {
        const body = `{"client_id":"${process.env.TEST_CLIENT_ID}","client_secret":"${process.env.TEST_CLIENT_SECRET}","audience":"${cfg.auth0audience}","grant_type":"client_credentials"}`;
        const authUrl = `https://${cfg.auth0Domain}/oauth/token`;
        logger.debug(`authenticating against ${authUrl}`, body.replace(process.env.TEST_CLIENT_SECRET, '*****'));
        return got.post(authUrl, { 
            headers: {
                'content-type': 'application/json'
            },
            timeout: 1000,
            body
        })
        .catch(err => {
            logger.fatal('error!', err);
            throw new Error(`Error when performing authorization: ${err.message}`)
        })
        .then(response => {
            const token = JSON.parse(response.body).access_token;
            logger.debug('got access token:', token);
            return token;
        })        
    }

    function authenticate(options) {
        if (cfg.disableAuth) 
            return Promise.resolve(options);
        return getAuthToken()
        .then(token => addHeader("authorization", `Bearer ${token}`, options));
    }

    this.failOp = (verb, endpoint, options) => 
        doOp(verb, endpoint, options)
        .then(res => { throw new Error(res) })
        .catch(err => err.response);

    this.doOp = (verb, endpoint, options) => {
        const url = `http://${cfg.hostname}:${cfg.port}${endpoint}`;
        const opt = addHeader("content-type", "application/json", R.merge(options, { timeout: 1000 }));
        logger.debug(`performing a ${verb} against ${url} with options ${JSON.stringify(opt)}`);
        return authenticate(opt)
        .then(o => got[verb.toLowerCase()](url, o))
        .catch(err => {
            logger.fatal("got error:", err);
            throw err;
         });
    }  
}

defineSupportCode(function({setWorldConstructor}) {
  setWorldConstructor(CustomWorld)
});