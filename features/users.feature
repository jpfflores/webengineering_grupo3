Feature: User mgmt API

   As an API consumer
   I want to be able to manage user data

   Scenario: Add user
   Given a valid user payload
   When I perform a POST against the /users endpoint
   Then I receive a 201 status code
   And the payload for the new resource
   And I can GET it by id
   

   Scenario: Delete user
   Given an existing user with id 2780019b-b156-4061-820b-5c9c6da759e1
   When I perform a DELETE against the /users/2780019b-b156-4061-820b-5c9c6da759e1 endpoint
   Then I receive a 200 status code

   Scenario: Edit user
   Given an existing user with id 2780019b-b156-4061-820b-5c9c6da759e2
   And I want to update his/her name
   When I perform a PATCH against the /users/2780019b-b156-4061-820b-5c9c6da759e2 endpoint
   Then I receive a 200 status code
   And the user has the new name
   
   Scenario Outline: User listing
   Given a set of existing users
   And I want to filter them by <filter>
   When I perform a GET against the /users endpoint, using that filter
   Then I receive a 200 status code
   And a list of users that match the same <filter>

   Examples:
   | filter      |
   | city        |
   | subdivision |
   | country     |
