'use strict'

const factory = require('./src/factory');
factory 
    .resolveServer()
    .then(s => s.start());
