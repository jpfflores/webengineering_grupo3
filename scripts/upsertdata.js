#!/usr/bin/env node

const data = require('./sampledata.json');
const MongoClient = require('../api/node_modules/mongodb').MongoClient;
const url = 'mongodb://localhost:27017/myproject';

var insertInCollection = function(db, col, callback) {
  var collection = db.collection(col);
  const records = data[col];
  console.log(`inserting the following records in ${col}:`, records)
  collection.insertMany(records, function(err, result) {
    if (err) { console.log('error inserting:', err); return callback(err); }
    console.log("Inserted documents in the users collection");
    callback(result);
  });
}

var insertDocuments = function(db, callback) {
    insertInCollection(db, 'users', callback);
}


MongoClient.connect(url, function(err, db) {
  if (err) {
      console.log('error connecting', err);
      return 1;
  }
  console.log("Connected correctly to server");
 
  insertDocuments(db, function() {
    db.close();
  });
});

/*
var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');
 
// Connection URL 
var url = 'mongodb://localhost:27017/myproject';
// Use connect method to connect to the Server 
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");
 
  insertDocuments(db, function() {
    db.close();
  });
});
*/